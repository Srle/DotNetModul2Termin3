﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Mvc;
using Knjizara.Models;

namespace Knjizara.Controllers
{
    public class BookstoreController : Controller
    {
        public static List<Book> bookList1 = new List<Book>();
        public Bookstore bookStore = new Bookstore(1, "Knjizara1", bookList1);

        public ActionResult Index()
        {
            StringBuilder sb = new StringBuilder();
            
            sb.Append("<hr/>");
            sb.Append("<h1>Bookstore - Home page</h1>");
            sb.Append("<hr/>");
            sb.Append("<form method='get' action='/Bookstore/AddBook'>");
            sb.Append("<table border='1'>");
            sb.Append("<tr><th colspan='2'>Add new book</th></tr>");
            sb.Append("<tr><th align='left'> Name of book:</th><td><input type='text' required name='NameOfBook' /></td></tr>");
            sb.Append("<tr><th align='left'> Price of book:</th><td><input type='number' required name='PriceOfBook' /></td></tr>");
            sb.Append("<tr><th align='left'> Genre of book:</th><td><select name='GenreOfBook'><option value ='Science'> Science </option>");
            sb.Append("<option value ='Comedy'>Comedy</option><option value='Horror'>Horror</option></select></td></tr> ");
            sb.Append("<tr><th colspan='2' align='right'><input type='submit' value='Add'/></th></tr>");
            sb.Append("</table>");
            sb.Append("</form>");
            sb.Append("<a href='/Bookstore/List'>Show all books</a> / ");
            sb.Append("<a href='/Bookstore/Deleted'>Show deleted books</a>");
            return Content(sb.ToString());
        }

        [HttpGet]
        public ActionResult AddBook (string NameOfBook, double PriceOfBook, string GenreOfBook)
        {
            int id = bookList1.Count + 1;
            Book bk = new Book(id, NameOfBook, PriceOfBook, GenreOfBook, false);
            bookList1.Add(bk);

            //StringBuilder sb = new StringBuilder();
            //sb.Append("<h1>Added:</h1>");
            //sb.Append("<p>Name: " + NameOfBook + "</p>");
            //sb.Append("<p>Price: " + PriceOfBook + "</p>");
            //sb.Append("<p>Genre: " + GenreOfBook + "</p>");
            //sb.Append("<a href='Index'>Homepage</a>");
            //return Content(sb.ToString());

            return RedirectToAction("List");         
        }

		public ActionResult List()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<h1>List of books</h1>");
            sb.Append("<table border='1'>");
            sb.Append("<tr>");
            sb.Append("<th>Id</th>");    
            sb.Append("<th>Name</th>");      
            sb.Append("<th>Price</th>"); 
            sb.Append("<th>Genre</th>");
            sb.Append("</tr>");
            foreach (Book b in bookList1)
            {
               if(!b.Deleted)
			   {
					sb.Append("<tr>");
					sb.Append("<td>" + b.Id + "</td>");
					sb.Append("<td>" + b.Name + "</td>");
					sb.Append("<td>" + b.Price + "</td>");
					sb.Append("<td>" + b.Genre + "</td>");
					sb.Append("<td>");
					sb.Append("<a href='Delete?id=" + b.Id + "'>delete </a>");
					sb.Append("</td>");            
					sb.Append("</tr>");
				}
            } 
            sb.Append("</table>");
            sb.Append("<a href='Index'>Homepage</a>");

            return Content(sb.ToString());
        }
			
		public ActionResult Delete(int id)
        {
            for (int i = 0; i < bookList1.Count; i++)
            {
                if (bookList1[i].Id == id)
                {
                    bookList1[i].Deleted = true;
                }
            }
            //StringBuilder sb = new StringBuilder();
            //sb.Append("<p>id:" + id + "</p>");
            //sb.Append("<a href='Index'>Homepage</a>");
            //return Content(sb.ToString());

            return RedirectToAction("Deleted");
        }
	
        public ActionResult Deleted()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<h1>List of deleted books</h1>");
            sb.Append("<table border='1'>");
            sb.Append("<tr>");
	        sb.Append("<th>Id</th>");    
            sb.Append("<th>Name</th>");      
            sb.Append("<th>Price</th>"); 
            sb.Append("<th>Genre</th>");			
            sb.Append("</tr>");
            foreach (Book b in bookList1)
            {
                if (b.Deleted)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + b.Id + "</td>");
                    sb.Append("<td>" + b.Name + "</td>");
                    sb.Append("<td>" + b.Price + "</td>");
                    sb.Append("<td>" + b.Genre + "</td>");
                    sb.Append("</tr>");
                }
            }			
            sb.Append("</table>");
            sb.Append("<a href='Index'>Homepage</a>");

            return Content(sb.ToString());
        }
		
    }
}
