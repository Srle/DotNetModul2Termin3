﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knjizara.Models
{
    public class Book
    {
		public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Genre { get; set; }
        public bool Deleted { get; set; }
		
		public Book()
        {
        }

        public Book(int id, string name, double price, string genre, bool deleted)
        {
            this.Id = id;
            this.Name = name;
            this.Price = price;
            this.Genre = genre;
            this.Deleted = deleted;
        }
			
    }
	
	
	
	
	
	
	
}