﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Knjizara.Models
{
    public class Bookstore
    {
		public int Id { get; set; }
        public string Name { get; set; }
        public List<Book> Books { get; set; }

        public Bookstore()
        {
        }

        public Bookstore(int id, string name)
        {
            Books = new List<Book>();
            this.Id = id;
            this.Name = name; 
        }

		    public Bookstore(int id, string name, List<Book> books)
        {
            this.Id = id;
            this.Name = name; 
			this.Books = books;
        }
    }
 
}